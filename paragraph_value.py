import argparse
import keras
from keras_retinanet import models
from keras_retinanet.utils.image import read_image_bgr, preprocess_image, resize_image
import glob
import numpy as np
import itertools
import shutil
from utils import *
import Paragraph_to_Dictionary
import ast
import math
import copy
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import json
import os
        

''' Model Path '''
model_path = '/home/user/project/corpus_paragraph/model/26Jun.h5'
retina_model = models.load_model(model_path, backbone_name='resnet101')
labels_to_names = {0: 'box', 1: 'table', 2:'column', 3:'header'}

pdf_images_dir = 'pdf_images/'
output_dir = 'Output/'


def get_table_header(pdf_images_dir):
    image_dir = glob.glob(pdf_images_dir+'*Temp.jpg')
    retina_array = []
    table_header_coord = []
    resize_pdf_images(image_dir)
    for images in image_dir:
        retina_image = read_image_bgr(images)
        retina_image = preprocess_image(retina_image)
        retina_image, scale = resize_image(retina_image, min_side=1200, max_side=2000)
        retina_array.append(retina_image)

    retina_array = np.asarray(retina_array)
    boxes, scores, labels = retina_model.predict_on_batch(retina_array)
    boxes /= scale
    boundary_buffer = 2
    for i in range(len(image_dir)):
        header_list, table_list = [], []
        image_cv = cv2.imread(image_dir[i])
        img_h, img_w = image_cv.shape[:2]
        for box, score, label in zip(boxes[i], scores[i], labels[i]):
            if score < 0.5:
                break
            b = box.astype(int)
            x1, y1, x2, y2 = b[0], b[1], b[2], b[3]
            x1 = 0 if x1 < 0 else (x1 + boundary_buffer)
            y1 = 0 if y1 < 0 else (y1 + boundary_buffer)
            x2 = img_w if x2 > img_w else (x2 - boundary_buffer)
            y2 = img_h if y2 > img_h else (y2 - boundary_buffer)
            if label == 3:
                header_list.append([x1, y1, x2, y2])
            if label == 1:
                table_list.append([x1, y1, x2, y2])
	
        if len(table_list) < 1 and len(header_list) < 1:
            table_header_coord.append([[], []])
        if len(table_list) > 0:
        	table_coord = non_max_suppression(np.asarray(table_list)).tolist()
        else:
        	table_coord = []
        if len(header_list) > 0:
        	header_coord = non_max_suppression(np.asarray(header_list)).tolist()
        else:
        	header_coord = []
        table_header_coord.append([table_coord, header_coord, image_dir[i]])
    return table_header_coord


#def process(file_pathh,pagee,pd_folder_path):
if __name__ == "__main__":
    f = open('/home/user/project/corpus_paragraph/json_keyword_page/j1.json',) 
    inp_json=json.load(f) 


    temp_d=dict()
    for each in inp_json.keys():
        if each not in temp_d.keys():
            temp_d[each]=list()
        for keyword,pages in inp_json[each].items():
            for each_elem in pages:
                temp_d[each].append(each_elem["Page From"])
                temp_d[each].append(each_elem["Page To"])
                
    print(temp_d)
    for each in temp_d.keys():
        temp_d[each].append(min(temp_d[each]))
        temp_d[each].append(max(temp_d[each]))
    print(temp_d)

    for each in temp_d.keys():
        file_pathh='/home/user/project/corpus_paragraph/data/new/'+each
        pd_folder_path='/home/user/project/corpus_paragraph/Output/'+file_pathh[(file_pathh.rfind('/') + 1) : -4]
        pagee=[]
        if len(temp_d[each])>2:
            from_page=temp_d[each][-2]
            to_page=temp_d[each][-1]
        
            for pg_idx in range(from_page,to_page+1):
                pagee.append(pg_idx)
            print(each,from_page,to_page,pagee)



            page_num=pagee
            pdf_file=file_pathh


            print('page_num= ',page_num)
            print('pdf_file= ',pdf_file)
            print('model= 26jun')


            if not os.path.exists(output_dir):
                os.makedirs(output_dir)

            pdf_name = pdf_file[(pdf_file.rfind('/') + 1) : -4]
            final_output_path = output_dir + pdf_name + '/'
            if not os.path.exists(final_output_path):
                os.makedirs(final_output_path)

            # Get the images out of pdf
            get_pdf_images(pdf_images_dir, pdf_file, page_num)
            # Get the table and header coordinates
            table_header_coord = get_table_header(pdf_images_dir)



            #final json declaring
            #-------------------------
            count=0

            final_dict={"data":[{"page_number":"","value":[{'paragraph':"",'coordinate':{}}]}]}
            final_dict['data'].pop(0)
            pg_idx=0
            for table_header in table_header_coord:
                if len(table_header) != 3:
                    continue
                table_coord_list = table_header[0]
                header_coord_list = table_header[1]
                orig_image_path = table_header[2].replace('Temp.jpg', '.jpg')
                image_name = orig_image_path[(orig_image_path.rfind('/') + 1) : ]
                # Copying the file from 'pdf_images' directory to 'Output' directory
                shutil.copy(orig_image_path, final_output_path + image_name)
                image_cv = cv2.imread(final_output_path + image_name)

                '''<--- Work with header and table coord here --->'''
                padding_x = 150
                padding_y = 50

                
                im_name=image_name.split('.')[0]

                
                for (header_coord, table_coord) in itertools.zip_longest(table_coord_list, header_coord_list):
                    print('table_coord= ',table_coord)
                    print('Header_coord= ',header_coord)
                    #appending into dictionary
                    if header_coord is not None:
                        cv2.rectangle(image_cv, (header_coord[0] - padding_x, header_coord[1] - padding_y),
                                      (header_coord[2] + padding_x, header_coord[3] + padding_y), (255, 255, 255), -1)
                                    

                    if table_coord is not None:
                        cv2.rectangle(image_cv, (table_coord[0] - padding_x, table_coord[1] - padding_y),
                                      (table_coord[2] + padding_x, table_coord[3] + padding_y), (255, 255, 255), -1)
                        
                cv2.imwrite(final_output_path + image_name, image_cv)
                

            
                if count==0:
                    for page_img in range(len(page_num)):
                        paragraph_data =Paragraph_to_Dictionary.get_paragraph(pd_folder_path)
                else:
                    paragraph_data =Paragraph_to_Dictionary.get_paragraph(pd_folder_path)
                count=count+1
                
                
                paragraph_data_dict=paragraph_data[im_name]
                
                
                page_no=im_name.split('_')[-1]
                page_no=page_no.split('Page')[-1]
                page_no=int(page_no)
                
                temp_dict={"page_number":"","value":[]}
                final_dict['data'].append(temp_dict)
                final_dict['data'][pg_idx]['page_number']=page_no

                
                for dict_idx in range(len(paragraph_data_dict)):
                    final_dict['data'][pg_idx]['value'].append({'paragraph':"",'coordinate':{}})
                    final_dict['data'][pg_idx]['value'][dict_idx]['paragraph']=paragraph_data_dict[dict_idx]['value']
                    final_dict['data'][pg_idx]['value'][dict_idx]['coordinate']=paragraph_data_dict[dict_idx]['coordinate']

                pg_idx=pg_idx+1
            

            #saving json
            o_file='/home/user/Desktop/2019-Annual-Report Telstra-pg_10_11_12.txt'
            with open(o_file, "w") as outfile:
                outfile.write(str(final_dict))

            #creating new json by applying corpus
            #-------------------------------------------
            
            corpus_json={'pdf-meta':{"name":"","author":"","modified_date":"","creation_date":"","pages":""},
                            "data":[{"page_num":"","page_data":[{"keyword":"","values":[{"value":"","coordinate":{}}]}]}]}

            domains=[]
            corpus_json['data'].pop(0)
            meta_name=pdf_file.split('/',-1)[-1]
            meta_name=meta_name.split('.')[0]
            corpus_json['pdf-meta']['name']=meta_name
            corpus_json['pdf-meta']['pages']=str(page_num)


            for idx in range(len(final_dict['data'])):
                domains=[]
                corpus_json['data'].append({"page_num":"","page_data":[]})

                corpus_json['data'][idx]['page_num']=final_dict['data'][idx]['page_number']
                
                print('page_number=>',final_dict['data'][idx]['page_number'])


                g=inp_json[each]
                for all in g.keys():
                    if final_dict['data'][idx]['page_number'] >= g[all][0]['Page From'] and final_dict['data'][idx]['page_number'] <= g[all][0]['Page To']:
                        if all not in domains:
                            domains.append(all)



                
                for domain_val in domains:
                    corpus_json['data'][idx]['page_data'].append({"keyword":"","values":[]})
                    corpus_json['data'][idx]['page_data'][-1]['keyword']=domain_val
                    par_count=0
                    for val_idx in range (len(final_dict['data'][idx]['value'])):
                        ratio=fuzz.partial_ratio(final_dict['data'][idx]['value'][val_idx]['paragraph'],domain_val)
                        if ratio>=80:
                            par_count=par_count+1
                            corpus_json['data'][idx]['page_data'][-1]['values'].append({"value":"","Paragraph":"","coordinate":{}})
                            corpus_json['data'][idx]['page_data'][-1]['values'][-1]['value']=final_dict['data'][idx]['value'][val_idx]['paragraph']
                            corpus_json['data'][idx]['page_data'][-1]['values'][-1]['coordinate']=final_dict['data'][idx]['value'][val_idx]['coordinate']
                            corpus_json['data'][idx]['page_data'][-1]['values'][-1]['Paragraph']=par_count
                            
            

            o_file='/home/user/Desktop/'+file_pathh[(file_pathh.rfind('/') + 1) : -4]+'1.txt'
            with open(o_file, "w") as outfile:
                outfile.write(str(corpus_json))

            print(pdf_name,corpus_json)